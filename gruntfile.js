module.exports = function(grunt) {
	
	// Configure task(s)
	grunt.initConfig({
		
		pkg: grunt.file.readJSON('package.json'),
		
		compass: {                 
   			build: {                   
      			options: {              
        			sassDir: 'scss',
        			cssDir: 'css',
       				benvironment: 'production'
      			}
    		},
    	
    		dev: {                    
      			options: {
      				config: 'config.rb',
        			sassDir: 'scss',
        			cssDir: 'css'
      			}
    		}
  		},

  		watch: {
			css: {
				files: ['scss/**/*.scss'],
				tasks: ['compass:dev']
			},
			livereload: {
      			options: { livereload: true },
      			files: ['css/**/*'],
   			 }
		},
		
	});
	
	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');


	
	//Register task(s)
	grunt.registerTask('default', ['compass:dev']);
	grunt.registerTask('build', ['compass:build']);
}